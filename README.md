# Name of library

## Description




## Installation

### from gitlab

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/hestia_building.git
```

Ou

```
pip install git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/hestia_building.git
```

### test install

Get install directory
```
pip show 
```

Use pytest on hades_stats path
```
pytest --doctest-modules /path_wherre_pip_install/site-packages/
```

## Usage


```

```

## License
GNU GPL

