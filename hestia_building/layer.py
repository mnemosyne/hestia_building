"""
 Material and Layer Classes
  - set thermal properties
  - thermal relations, conductive transfert
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

import numpy
# ~ import pandas

from apollon_physics import PhysicConstant
from chaos_substances import PROPERTIES_TABLE, SPECIFIC_MASS, SPECIFIC_HEAT_CAPACITY, THERMAL_CONDUCTIVITY, SW_REFLECTIVITY, SW_TRANSMISSIVITY,IR_EMISSIVITY, WATER_VAPOUR_PERMEABILITY, WV_PERMEABILITY_OF_AIR

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

def _expand_alias(al):
    """
    Expand all aliases see code below
    """
    res = [al, al.replace(" ", "_"), al.replace(" ", "-")]
    res += [i.upper() for i in res]
    res += [i.lower() for i in res]
    return res


EXPECTED_PROPERTIES = (SPECIFIC_MASS, SPECIFIC_HEAT_CAPACITY, THERMAL_CONDUCTIVITY, SW_REFLECTIVITY , SW_TRANSMISSIVITY, IR_EMISSIVITY, WATER_VAPOUR_PERMEABILITY)

_nm_alias = {k: [] for k in EXPECTED_PROPERTIES}
_nm_alias[SPECIFIC_MASS] = ["rho", "density"]
_nm_alias[SPECIFIC_HEAT_CAPACITY] = ["cp"]
_nm_alias[THERMAL_CONDUCTIVITY] =  ["k", "lambda", "conductivity"]
_nm_alias[WATER_VAPOUR_PERMEABILITY] = ["permeability", "wv_permeability"]
_nm_alias[IR_EMISSIVITY] = ["epsilon"]
            
_nm_alias = {k: v + _expand_alias(k) for k, v in _nm_alias.items()}

NM_ALIAS = {i:k for k,v in _nm_alias.items() for i in v}



##======================================================================================================================##
##                FUNCTION                                                                                              ##
##======================================================================================================================##


    
    



##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##
# See there ~ https://en.wikipedia.org/wiki/Heat_transfer

# ~ Thermal Contact is when two substances can affect each other's temperature.
# ~ Thermal Equilibrium is when two substances in thermal contact no longer transfer heat.
# ~ Thermal Expansion takes place when a substance expands in volume as it gains heat. Thermal contraction also exists.
# ~ Conduction is when heat flows through a heated solid.
# ~ Convection is when heated particles transfer heat to another substance, such as cooking something in boiling water.
# ~ Radiation is when heat is transferred through electromagnetic waves, such as from the sun.
# ~ Insulation is when a low-conducting material is used to prevent heat transfer.



class Material:
    """ Class doc
    >>> m = Material("iron")
    >>> m.thermal_conductivity
    80.0
    >>> Material(other=m).thermal_conductivity
    80.0
    
    >>> m = Material("liquid water")
    >>> m.thermal_conductivity
    nan
    >>> m.specific_mass
    1000.0
    >>> m.specific_heat_capacity
    4187.0
    
    
    >>> m = Material("light wood")
    >>> m.thermal_conductivity
    0.14
    >>> m.specific_mass
    500.0
    >>> m.specific_heat_capacity
    nan

    >>> m = Material("biofib trio")
    >>> m.thermal_conductivity
    0.038
    >>> m.specific_mass
    30.0
    >>> m.specific_heat_capacity
    1800.0


    >>> m = Material("proclimat db+")
    >>> m.thermal_conductivity
    0.13
    >>> m.specific_mass
    800.0
    >>> m.specific_heat_capacity
    nan

    >>> m = Material("concrete block")
    >>> m.thermal_conductivity
    0.95
    >>> m = Material("concrete block", thermal_conductivity=0.7)
    >>> m.thermal_conductivity
    0.7
    >>> m.thermal_effusivity()
    
    >>> m.thermal_diffusivity()
    >>> m = Material("air")
    >>> m.thermal_conductivity
    0.0262
    >>> m.specific_mass
    nan
    >>> m.specific_heat_capacity
    nan


    
    
    """
    
    
    
    def __init__(self, name=None, other=None,**kwargs):
        """ Class initialiser """
        properties = {k: numpy.nan for k in EXPECTED_PROPERTIES}
        
        if kwargs:
            kwargs2 = {NM_ALIAS[k]: v for k, v in kwargs.items()}
            assert all(k in EXPECTED_PROPERTIES for k in kwargs2)
        else:
            kwargs2 = {}
            
        if isinstance(name, str):
            # ~ assert not kwargs, "if name is given, no kwargs.\n Got {0}".format(kwargs)
            assert name in PROPERTIES_TABLE.index, f"{name} not available in{sorted(PROPERTIES_TABLE.index)}"
            new_kwargs = PROPERTIES_TABLE.loc[name]
            new_kwargs = dict(new_kwargs)
            new_kwargs.update(kwargs2)
        elif isinstance(other, Material):
            # ~ to_get = [k for k in other.__dict__.keys() if k in NM_ALIAS]
            # ~ new_kwargs = {NM_ALIAS[k]: other.__dict__[k] for k in to_get}
            # ~ assert all([k in EXPECTED_PROPERTIES for k in new_kwargs])
            new_kwargs = {k: v for k, v in other.__dict__.items() if k in EXPECTED_PROPERTIES}
            new_kwargs.update(kwargs2)
                
        elif kwargs:
            new_kwargs = kwargs2
        else:
            raise ValueError(f"Dont knwo what do do with {name} and {kwargs}")
        
        
        properties.update(new_kwargs)
        
        #GET variables 
        self.specific_mass = properties[SPECIFIC_MASS] #rho kg/m3   , density (more precisely, the volumetric mass density; also known as specific mass) 
        
        #thermal transfert
        self.specific_heat_capacity = properties[SPECIFIC_HEAT_CAPACITY] #cp J/kg/K
        self.thermal_conductivity = properties[THERMAL_CONDUCTIVITY] #thermal_conductivity lambda, The conventional symbol for thermal conductivity is k. 
        
        #water vapour transfert
        self.water_vapour_permeability = properties[WATER_VAPOUR_PERMEABILITY]
        
        #Radiation
        self.ir_emissivity = properties[IR_EMISSIVITY]
        self.sw_reflectivity = properties[SW_REFLECTIVITY]
        self.sw_transmissivity = properties[SW_TRANSMISSIVITY]
        
        
        assert all(i in self.__dict__ for i in EXPECTED_PROPERTIES)
        
        #calculated variables
        self.volumetric_heat_capacity = PhysicConstant(self.specific_mass * self.specific_heat_capacity, unit = "J/K/m^3")
        
        # ~ https://en.wikipedia.org/wiki/Volumetric_heat_capacity
        # ~ The volumetric heat capacity of a material is the heat capacity of a sample of the substance divided by the volume of the sample. 
        # ~ Informally, it is the amount of energy that must be added, in the form of heat, to one unit of volume of the material in order to cause an increase of one unit in its temperature. 
        # ~ The SI unit of volumetric heat capacity is joule per kelvin per cubic meter, J/K/m3 or J/(K·m3). 
        
        
        
    def thermal_resistivity(self):
        """
        See main doctest
        """
        val = 1 / self.thermal_conductivity
        res = PhysicConstant(val, unit = "mK/W")
        return res
        
    def thermal_diffusivity(self):
        """
        https://en.wikipedia.org/wiki/Heat_equation
        In heat transfer analysis, thermal diffusivity is the thermal conductivity divided by density and specific heat capacity at constant pressure.
        It measures the rate of transfer of heat of a material from the hot end to the cold end. 
        It has the SI derived unit of m²/s.
        """
        val = self.thermal_conductivity / self.volumetric_heat_capacity
        res = PhysicConstant(val, unit = "m^2/s")
        return res
        
    def thermal_effusivity(self):
        """
        https://en.wikipedia.org/wiki/Thermal_effusivity
        In thermodynamics, the thermal effusivity, thermal inertia or thermal responsivity of a material is defined as the square root of the product of the material's thermal conductivity and its volumetric heat capacity.
        A material's thermal effusivity is a measure of its ability to exchange thermal energy with its surroundings.
        """
        val = (self.thermal_conductivity * self.volumetric_heat_capacity)**0.5
        res = PhysicConstant(val,unit = "J/m^2/K/s^0.5")
        return res
    
    
    def water_vapour_resistivity(self):
        """
        Function doc
        >>> "To do"
        """
        
    
    
    
    
    def thermal_contact(self, other):
        """
        Function doc
        >>> "To do"
        """
        
        
        
    def summary(self):
        """
        Function doc
        >>> "To do"
        """

        
        



class Layer(Material): 
    """
    ## Check resistance and permeance #data from https://www.biofib.com
    >>> bfc = Layer(name='biofib chanvre', thickness=0.1)
    >>> bfc.specific_heat_capacity
    1800.0
    >>> bfc.specific_mass
    40.0
    >>> bfc.thermal_resistance
    2.5
    >>> bfc.sd
    0.1
    >>> bfc.water_vapour_resistance_coef
    1.0
    >>> bfc.grad_t(10)
    100.0
    
    >>> bft = Layer(name='biofib trio', thickness=0.1)
    >>> bft.thermal_conductivity
    0.038
    >>> bft.specific_heat_capacity
    1800.0
    >>> bft.specific_mass
    30.0
    >>> round(bft.thermal_resistance,2)
    2.63
    >>> round(bft.sd, 2)
    0.15
    >>> round(bft.water_vapour_resistance_coef, 2)
    1.5
    
    >>> bft = Layer(name='biofib trio', thickness=0.2)
    >>> bft.specific_heat_capacity
    1800.0
    >>> bft.specific_mass
    30.0
    >>> round(bft.thermal_resistance,2)
    5.26
    >>> round(bft.sd, 2)
    0.3
    >>> round(bft.water_vapour_resistance_coef, 2)
    1.5
    
    >>> bfp = Layer(name='biofib pano', thickness=0.035)
    >>> bfp.thermal_conductivity
    0.045
    >>> bfp.specific_heat_capacity
    2100.0
    >>> bfp.specific_mass
    210.0
    >>> round(bfp.thermal_resistance,2)
    0.78
    >>> round(bfp.sd, 2)
    0.1
    >>> round(bfp.water_vapour_resistance_coef, 2)
    3.0
    
    ##https://www.materiaux-naturels.fr/doc/product/n_148.pdf
    
    >>> db = Layer(name='proclimat db+', thickness=0.23e-3)
    >>> round(db.water_vapour_resistance_coef, 0)
    10000.0
    >>> round(db.sd, 2)
    2.3
    
    ## Examples found there: https://www.engineeringtoolbox.com/conductive-heat-transfer-d_428.html
    >>> iron = Layer(thickness=0.05, area=1, thermal_conductivity=70)
    >>> print(iron.conductive_heat_transfer(70))
    98000.0 W
    >>> iron = Layer(name="iron", area=1, thickness=0.05)
    >>> iron.conductive_heat_transfer(70)
    112000.0
    
    >>> linner = Layer(thickness=0.012, area=1, thermal_conductivity=19)
    >>> loutside = Layer(thickness=0.05, area=1, thermal_conductivity=0.7)
    >>> eqlayer = linner & loutside
    >>> round(eqlayer.thermal_conductivity, 3)
    0.86
    >>> round(eqlayer.thermal_resistance, 3)
    0.072
    >>> round(eqlayer.conductive_heat_transfer(800-350), 0)
    6245.0
    
    
    
    """
    
    def __init__(self, thickness=None, area=1, name=None, other=None, **kwargs):
        """ Class initialiser """
        
        #geometry infos
        if other is None:
            self.thickness = PhysicConstant(thickness, unit="m")
            self.area = PhysicConstant(area, unit="m^3")
        else:
            self.thickness = other.thickness
            self.area = other.area

        assert self.thickness > 0 and self.area > 0
        self.volume = PhysicConstant(self.thickness * self.area, unit="m^3")
        
        
        #propetries infos
        Material.__init__(self, name=name, other=other, **kwargs)
        
        
        #computed properties
        
        self.mass = PhysicConstant(self.specific_mass * self.volume, unit="kg")
        self.heat_capacity = PhysicConstant(self.specific_heat_capacity * self.mass, unit="J/K")
        
        self.thermal_resistance = PhysicConstant(value=self.thickness / self.thermal_conductivity, unit="m^2*K/W")
        self.thermal_conductance = PhysicConstant(value=self.thermal_conductivity / self.thickness, unit="W/m^2/K")
        
        self.water_vapour_permeance = PhysicConstant(value=self.water_vapour_permeability / self.thickness, unit="kg-wv/s/m^2/Pa")
        self.water_vapour_transfert_coef = PhysicConstant(value=self.water_vapour_permeance * self.area , unit="kg-wv/s/Pa")
        
        self.conductive_heat_transfer_coef = PhysicConstant(value=self.thermal_conductance * self.area, unit="W/K")

        if self.water_vapour_permeability == 0:
            self.water_vapour_resistance = PhysicConstant(numpy.inf, unit="s*m^2*Pa/kg-wv")    
            self.water_vapour_resistance_coef = PhysicConstant(numpy.inf, unit="-")
            self.sd = PhysicConstant(numpy.inf, unit="m eq of air")
        else:
            self.water_vapour_resistance = PhysicConstant(value=self.thickness / self.water_vapour_permeability, unit="s*m^2*Pa/kg-wv")    
            self.water_vapour_resistance_coef = PhysicConstant(WV_PERMEABILITY_OF_AIR / self.water_vapour_permeability, unit="-")
            self.sd = PhysicConstant(self.thickness*self.water_vapour_resistance_coef, unit="m eq of air")
    
        
    def __and__(self, other):
        """
        See main doctest
        """
        assert abs(self.area - other.area) < 1e-8, "layer must have the same area"
        
        #basic variables
        thickness = self.thickness + other.thickness
        
        
        #intermediate, to be check after
        mass = self.mass + other.mass
        vol = self.volume + other.volume
        ht_capacity = self.heat_capacity + other.heat_capacity
        eq_th_resistance = self.thermal_resistance + other.thermal_resistance
        
        #water vapor
        eq_wv_resistance = self.water_vapour_resistance + other.water_vapour_resistance

        #entries to new Layer
        new_rho = mass / vol        
        eq_cp = ht_capacity / mass        
        eq_th_cond = thickness / eq_th_resistance
        eq_wv_perm = thickness / eq_wv_resistance
        
        res = Layer(area=self.area, thickness=thickness, specific_heat_capacity=eq_cp, specific_mass=new_rho, thermal_conductivity=eq_th_cond, water_vapour_permeability=eq_wv_perm)
        
        _check_dic = dict(volume=(vol, res.volume), mass=(mass, res.mass), ht_capacity=(ht_capacity, res.heat_capacity), eq_th_resistance=(eq_th_resistance, res.thermal_resistance), eq_wv_resistance=(eq_wv_resistance, res.water_vapour_resistance))
        for k, vs in _check_dic.items():
            v0, v1 = vs
            assert (numpy.isnan(v0) and numpy.isnan(v1)) or (v0==v1) or abs(v0 - v1)/(v0) < 1e-8, f"pb {k}: {v0} vs {v1}"
        
        return res
        
        
        
    def __or__(self, other):
        """
        >>> "To do"
        """
                
        
    def grad_t(self, delta_t):
        """
        See main doctest
        """
        res = delta_t /self.thickness
        return res
        
    def grad_e(self, delta_e):
        """
        Function doc
        >>> 'afarie'
        """
        res = delta_e /self.thickness
        return res

    def conductive_heat_transfer(self, delta_t, per_m2=False):
        """
        See main doctest
        """
        if per_m2:
            res = self.thermal_conductance * delta_t
            res = PhysicConstant(res, unit="W/m2")
            assert abs(res - self.thermal_conductivity * self.grad_t(delta_t=delta_t))/res < 1e-8
        else:
            res = self.conductive_heat_transfer_coef * delta_t
            res = PhysicConstant(res, unit="W")
            assert abs(res - self.area * self.conductive_heat_transfer(delta_t=delta_t, per_m2=True))/res < 1e-8
        
        return res
        
    def water_vapour_transfert(self, delta_e, per_m2=False):
        """
        Function doc
        >>> 'afarie'
        """
        if per_m2:
            res = self.water_vapour_permeance * delta_e
            res = PhysicConstant(res, unit="kg-wv/s/m2")
            assert abs(res - self.water_vapour_permeability * self.grad_e(delta_e=delta_e))/res < 1e-8
        else:
            res = self.water_vapour_transfert_coef * delta_e
            res = PhysicConstant(res, unit="kg-wv/s")
            assert abs(res - self.area * self.water_vapour_transfert(delta_e=delta_e, per_m2=True))/res < 1e-8
        
        return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)




    m = Material("air")

    l = Layer(name="air", thickness=1)

    concrete = Layer(name="concrete", thickness=0.14)
    print(concrete.thermal_resistance)
