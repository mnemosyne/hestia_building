"""
 Some physics tools for buildings
    - Thermal transfert 
    - basic relations
    - glazing


>>> glazed_area = 1
    
>>> area1 = 1
>>> walls1 = Layer(thickness=0.2, name="concrete", area=area1) & UnventilatedAirLayer(thickness=0.05, area=area1) & Layer(thickness=0.05, name="polystyrene", area=area1) & Layer(thickness=0.02, name="gypsum board", area=area1) & InnerAir(area=area1) & OuterAir(area=area1)
    
>>> glazing = Glazing(nglass=2,area=glazed_area, air_thickness=6e-3)
>>> glazing2 = glazing & InnerAir(area=glazed_area) & OuterAir(area=glazed_area)

>>> round(walls1.thermal_resistance, 2)
1.98

>>> round(walls1.conductive_heat_transfer(10), 2)
5.06

>>> glazing.thermal_resistance
0.132

"""
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest


from hestia_building.layer import *
from hestia_building.airlayer import *
from hestia_building.glazing import *
from hestia_building.multilayers import *



##======================================================================================================================##
##                OLD FUNCTIONS                                                                                         ##
##======================================================================================================================##

# ~ def thermal_insulance(th_conduct, e):
    # ~ """
    # ~ lambda = th_conduct
    
    # ~ R value It is the thermal resistance of unit area of a material (m^2 K)/W
    # ~ >>> 'afarie'
    # ~ """
    # ~ res = e / th_conduct
    # ~ return res
    
# ~ def thermal_conductivity(th_insulance, e):
    # ~ """
    # ~ Thermal conductivity is measured in watts per meter-kelvin (W/(m⋅K))
    # ~ >>> 'afarie'
    # ~ """
    # ~ res = e / th_insulance
    # ~ return res

# ~ def main():
    # ~ """
    
    # ~ """
    


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
    
