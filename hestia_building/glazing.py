"""

"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

# ~ import pandas
import numpy

# ~ from physics import Temperature, Distance
from chaos_substances import CP_DRYAIR, THERMAL_CONDUCTIVITY, SPECIFIC_MASS, SPECIFIC_HEAT_CAPACITY, WATER_VAPOUR_PERMEABILITY, WV_PERMEABILITY_OF_AIR, GLASS_NM, LOW_EMISSIVITY_GLASS_NM, HEAT_ABSORBING_GLASS_NM, REFLECTING_GLASS_NM, IR_EMISSIVITY, SW_REFLECTIVITY, SW_TRANSMISSIVITY
# ~ from meteorology import DryAir

from hestia_building.layer import Layer
from hestia_building.airlayer import _thermal_resistance_nm, UnventilatedAirLayer, HORIZONTAL, InnerAir, OuterAir


##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

GLASS_TYPES = (GLASS_NM, LOW_EMISSIVITY_GLASS_NM, HEAT_ABSORBING_GLASS_NM, REFLECTING_GLASS_NM)


##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

class Glazing(Layer):
    """ Class doc
    Check with some example there: http://www.sfu.ca/phys/346/121/lecture_notes/lecture33_heat_loss.pdf
    
    >>> i = InnerAir()
    >>> o = OuterAir()
    >>> io = i&o
    >>> simple_pane = Glazing(nglass=1, glass_thickness=6e-3) 
    >>> simple_pane.thermal_resistance
    0.006
    >>> round((simple_pane & io).thermal_resistance, 3)
    0.176
    >>> simple_pane.thickness == 6e-3
    True
    
    
    >>> double_glazed = Glazing(nglass=2, air_thickness= 6e-3)
    >>> double_glazed.thermal_resistance
    0.132
    >>> round((double_glazed & io).thermal_resistance, 3)
    0.302
    
    >>> round(double_glazed.thickness, 4)
    0.018
    >>> double_glazed = Glazing(nglass=2, air_thickness= 12e-3)
    >>> double_glazed.thermal_resistance
    0.17
    >>> (double_glazed & io).thermal_resistance
    0.34
    >>> double_glazed.thickness == 24e-3
    True
    
    >>> triple_glazed = Glazing(nglass=3, air_thickness= 6e-3)
    >>> round(triple_glazed.thermal_resistance, 3)
    0.258
    >>> round((triple_glazed & io).thermal_resistance, 3)
    0.428
    >>> triple_glazed.thickness == 30e-3
    True
        
    """
    
    def __init__(self, glass_type=GLASS_NM, nglass=1, glass_thickness=6e-3, air_thickness=0, area=1, heat_flow_direction=HORIZONTAL, **kwargs):
        """ Class initialiser """
        assert glass_type in GLASS_TYPES
        
        if nglass == 1:
            Layer.__init__(self, name=glass_type, thickness=glass_thickness, area=area, **kwargs)
            
        else:
            air_layer = UnventilatedAirLayer(thickness=air_thickness, area=area, heat_flow_direction=heat_flow_direction)
            glass_layer = Layer(name=glass_type, thickness=glass_thickness, area=area, **kwargs)
            
            eq_layer = glass_layer
            for _ilayer in range(nglass - 1):
                eq_layer = eq_layer & air_layer & glass_layer
            
            # ~ print(eq_layer.thickness)
            
            keep_kwargs_from_glass = {k: glass_layer.__dict__[k] for k in (IR_EMISSIVITY, SW_REFLECTIVITY, SW_TRANSMISSIVITY)}
            
            Layer.__init__(self, other=eq_layer, **keep_kwargs_from_glass)
            
            
            for _k, _v1 in self.__dict__.items():
                if _k in keep_kwargs_from_glass:
                    _v2 = keep_kwargs_from_glass[_k]
                else:
                    _v2 = eq_layer.__dict__[_k]
                assert (numpy.isnan(_v1) and numpy.isnan(_v2)) or (_v1==_v2) or abs(_v1 - _v2) < 1e-8, f"pb {_k}: {_v1} vs {_v2}"
            
        
        
        not_refleted = 1 - self.sw_reflectivity
        reradiated = not_refleted - self.sw_transmissivity
        
        self.solar_heat_gain_coefficient =  not_refleted - reradiated/2
        
        
    def radiations_decomposition(self):
        """
        Function doc
        >>> 'afarie'
        """
        print("Solar Heat Gain Coefficient is the fraction of incident solar radiation admitted through a window, both directly transmitted and absorbed & re-radiated inward. The lower a window's solar heat gain coefficient, the less solar heat it transmits.")
        print("Thus t+a+e/2 = 1 - r - e/2")
        
    def solar_heat_gain(self, sw_radiation, shading_coef):
        """
        Function doc
        >>> 'afarie'
        """
        print("https://en.wikipedia.org/wiki/Solar_gain")
        print("Solar Heat Gain Coefficient (SHGC)")

        
    def shading_glass(self):
        """
        Function doc
        >>> 'afarie'
        """
        new_layer = window & closer





class Shutter(Layer): #volet
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass






##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    
    i = InnerAir()
    o = OuterAir()
    io = i&o
    double_glazed = Glazing(nglass=2, air_thickness= 6e-3)
    print(double_glazed.thermal_resistance)
    print((io&double_glazed).thermal_resistance)
    
