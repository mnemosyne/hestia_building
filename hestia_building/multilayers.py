"""

"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import pandas

from hestia_building.layer import Layer


##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

class MultiLayers(pandas.Series):
    """ Class doc
    >>> 'Todo' """
    
    def __init__(self):
        """ Class initialiser """
        pass

    def to_equivalent_layer():
        """
        
        """
        EquivalentLayer


class SeriesOfLayers(MultiLayers):
    """ Class doc
    >>> 'afarie' 
    """
    
    def __init__(self):
        """ Class initialiser """
        pass

    
class ParallelPanels(MultiLayers):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass
    
class EquivalentLayer(Layer):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
