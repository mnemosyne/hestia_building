"""
Class for AirLayer:
    - unventilated
    - inner and outer air layer
    - La transmission de la chaleur de l'air ambiant à une paroi et vice versa se fait à la fois par rayonnement et par convection.
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import pandas
import numpy

from measurement.measures import Temperature, Distance

from chaos_substances import CP_DRYAIR, THERMAL_CONDUCTIVITY, SPECIFIC_MASS, SPECIFIC_HEAT_CAPACITY, WATER_VAPOUR_PERMEABILITY, WV_PERMEABILITY_OF_AIR
from ouranos_meteorology import DryAir

from hestia_building.layer import Layer


##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

_rhodryair = DryAir(p=1015e2,t=Temperature(c=15).k).rho

_thermal_resistance_nm = "thermal_resistance"

_air_properties = {SPECIFIC_MASS: _rhodryair, SPECIFIC_HEAT_CAPACITY: CP_DRYAIR, WATER_VAPOUR_PERMEABILITY: WV_PERMEABILITY_OF_AIR}



UPWARD = "upward"
DOWNWARD = "downward"
HORIZONTAL = "horizontal"

HEAT_FLOW_DIRS = (UPWARD, HORIZONTAL, DOWNWARD)
#Carrefull thickness are in mm
_up = {0 : 0.00, 5 : 0.11, 7 : 0.13, 10 : 0.15, 15 : 0.16, 25 : 0.16, 50 : 0.16, 100 : 0.16, 300 : 0.16}
_hz = {0 :0.00, 5 : 0.11, 7 : 0.13, 10 :0.15, 15 :0.17, 25 :0.18, 50 :0.18, 100 : 0.18, 300 : 0.18}
_dn = {0 : 0.00, 5 : 0.11, 7 : 0.13, 10 :0.15, 15 : 0.17, 25 : 0.19, 50 : 0.21, 100 : 0.22, 300 : 0.23}

#transform thickness in m and dict in Series
UNVENTILATED_AIR_RESISTANCES = {k: {Distance(mm=i).m: j for i, j in dic.items()} for k, dic in zip(HEAT_FLOW_DIRS, [_up,_hz, _dn])}
UNVENTILATED_AIR_RESISTANCES = {k: pandas.Series(v) for k, v in UNVENTILATED_AIR_RESISTANCES.items()}
UNVENTILATED_AIR_RESISTANCES = pandas.DataFrame(UNVENTILATED_AIR_RESISTANCES, columns = HEAT_FLOW_DIRS)

del _up, _hz, _dn

print(UNVENTILATED_AIR_RESISTANCES)

# voir https://energieplus-lesite.be/donnees/enveloppe44/enveloppe2/resistance-thermique-des-couches-d-air/
#Thickness of air layer Thermal resistance m2⋅K/WDirection of heat flow mm  
# ~ 0                      0.00                      0.00                      0.00                      
# ~ 5                      0.11                      0.11                      0.11                      
# ~ 7                      0.13                      0.13                      0.13                      
# ~ 10                     0.15                      0.15                      0.15                      
# ~ 15                     0.16                      0.17                      0.17                      
# ~ 25                     0.16                      0.18                      0.19                      
# ~ 50                     0.16                      0.18                      0.21                      
# ~ 100                    0.16                      0.18                      0.22                      
# ~ 300                    0.16                      0.18                      0.23

#For inner and outer air layer see there: https://www.sis.se/api/document/preview/909393/
#Surface ResistanceDirection of heat flow(m2K / W)UpwardsHorizontalDownwardsRsi0.100.130.17Rse0.040.040.04
_r_air_out = 0.04
_r_air_in = {DOWNWARD: 0.17, UPWARD:0.1, HORIZONTAL:0.13}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

   



##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##



class AirLayer(Layer):
    """ Parent class """
    
    def __init__(self, thickness, area=1, **thermal_infos):
        """ Class initialiser """
        assert len(thermal_infos) == 1, f"exp additional thermal infos. Got\n{thermal_infos}"
        
        if THERMAL_CONDUCTIVITY in thermal_infos:
            th_cond = thermal_infos[THERMAL_CONDUCTIVITY]
        elif _thermal_resistance_nm in thermal_infos:
            th_cond = thickness / thermal_infos[_thermal_resistance_nm]
        else:
            raise ValueError(f"Expect {THERMAL_CONDUCTIVITY} or {_thermal_resistance_nm}, got {thermal_infos}")
        
        properties = _air_properties.copy()
        properties[THERMAL_CONDUCTIVITY] = th_cond
        
        Layer.__init__(self, thickness=thickness, area=area, **properties)
        

class UnventilatedAirLayer(AirLayer):
    """ 
    >>> al = UnventilatedAirLayer(thickness=1e-3)
    >>> round(al.thermal_resistance, 3)
    0.022
    >>> round(al.thermal_conductivity, 3)
    0.045
    >>> al = UnventilatedAirLayer(thickness=2e-3)
    >>> round(al.thermal_resistance, 3)
    0.044
    >>> round(al.thermal_conductivity, 3)
    0.045
    >>> al = UnventilatedAirLayer(thickness=5e-3)
    >>> round(al.thermal_resistance, 3)
    0.11
    >>> round(al.thermal_conductivity, 3)
    0.045
    >>> al = UnventilatedAirLayer(thickness=0.01)
    >>> round(al.thermal_resistance, 3)
    0.15
    >>> round(al.thermal_conductivity, 3)
    0.067
    >>> al = UnventilatedAirLayer(thickness=0.1)
    >>> round(al.thermal_resistance, 3)
    0.18
    >>> round(al.thermal_conductivity, 3)
    0.556
    >>> al = UnventilatedAirLayer(thickness=0.3)
    >>> round(al.thermal_resistance, 3)
    0.18
    >>> round(al.thermal_conductivity, 3)
    1.667
    >>> al = UnventilatedAirLayer(thickness=0.4)
    >>> round(al.thermal_resistance, 3)
    0.18
    >>> round(al.thermal_conductivity, 3)
    2.222
    
    >>> al = UnventilatedAirLayer(heat_flow_direction=DOWNWARD, thickness=1e-3)
    >>> round(al.thermal_resistance, 3)
    0.022
    >>> round(al.thermal_conductivity, 3)
    0.045
    >>> al = UnventilatedAirLayer(heat_flow_direction=DOWNWARD, thickness=0.01)
    >>> round(al.thermal_resistance, 3)
    0.15
    >>> round(al.thermal_conductivity, 3)
    0.067
    >>> al = UnventilatedAirLayer(heat_flow_direction=DOWNWARD, thickness=0.1)
    >>> round(al.thermal_resistance, 3)
    0.22
    >>> round(al.thermal_conductivity, 3)
    0.455
    """
    
    def __init__(self, thickness, area=1, heat_flow_direction=HORIZONTAL):
        """ Class initialiser """
        #get air resistance
        assert heat_flow_direction in HEAT_FLOW_DIRS, f"{heat_flow_direction} not in \n{HEAT_FLOW_DIRS}"
        
        air_resi_ts = UNVENTILATED_AIR_RESISTANCES[heat_flow_direction]
        val = numpy.interp(x=thickness, xp=air_resi_ts.index, fp=air_resi_ts.values, left=numpy.nan, right=None)
        
        AirLayer.__init__(self, thickness=thickness, area=area, **{_thermal_resistance_nm: val})
        
        
        



class InnerAir(AirLayer):
    """ Class doc
    >>> inair = InnerAir(heat_flow_direction=HORIZONTAL) 
    >>> round(inair.thermal_resistance, 2)
    0.13
    >>> round(inair.thickness, 4)
    0.007
    >>> round(inair.thermal_conductivity, 4)
    0.0538
    
    >>> inair = InnerAir(heat_flow_direction=UPWARD)
    >>> round(inair.thermal_resistance, 2)
    0.1
    >>> round(inair.thickness, 4)
    0.0045
    >>> round(inair.thermal_conductivity, 4)
    0.0455
    
    >>> inair = InnerAir(heat_flow_direction=DOWNWARD)
    >>> round(inair.thermal_resistance, 2)
    0.17
    >>> round(inair.thickness, 4)
    0.015
    >>> round(inair.thermal_conductivity, 4)
    0.0882
    
    """
    
    def __init__(self, area=1, heat_flow_direction=HORIZONTAL):
        """ Class initialiser """
        air_resi_ts = UNVENTILATED_AIR_RESISTANCES[heat_flow_direction]
        air_resi_target = _r_air_in[heat_flow_direction]
        
        thickness = numpy.interp(x=air_resi_target, xp=air_resi_ts.values, fp=air_resi_ts.index, left=numpy.nan, right=None)
        
        AirLayer.__init__(self, thickness=thickness, area=area, **{_thermal_resistance_nm: air_resi_target})
        
        assert abs(self.thermal_resistance - air_resi_target) < 1e-8
        # ~ print(self.thickness)


class OuterAir(AirLayer):
    """ Class doc
    >>> for i in HEAT_FLOW_DIRS:print(OuterAir(heat_flow_direction=i).thermal_resistance)
    0.04 m^2*K/W
    0.04 m^2*K/W
    0.04 m^2*K/W
    
    >>> oair = OuterAir()
    >>> round(oair.thickness, 4)
    0.0018
    >>> round(oair.thermal_conductivity, 4)
    0.0455
    
    
    
    """
    
    def __init__(self, area=1, heat_flow_direction=HORIZONTAL):
        """ Class initialiser """
        air_resi_ts = UNVENTILATED_AIR_RESISTANCES[heat_flow_direction]
        air_resi_target = _r_air_out
        
        thickness = numpy.interp(x=air_resi_target, xp=air_resi_ts.values, fp=air_resi_ts.index, left=numpy.nan, right=None)
        
        AirLayer.__init__(self, thickness=thickness, area=area, **{_thermal_resistance_nm: air_resi_target})
        
        assert abs(self.thermal_resistance - air_resi_target) < 1e-8
        # ~ print(self.thickness)
       








##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
